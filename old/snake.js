//console.log('Hello World');
var canvas = document.getElementById("myCanvas");
var ctx = canvas.getContext("2d");

var box = 50;
var snake = [[200, 200], [250, 200], [300, 200]];
var foodX = 300;
var foodY = 300;
var score = 3;
var direction = 'left';

//gameLoop();

//document.getElementById("score").innerHTML = score;
draw();
drawFood();

function gameLoop() {

    //Move Snake every 100ms
    setInterval(move, 100);
}

document.addEventListener('keydown', function(event) {

    if(event.keyCode == 37) {
        direction = 'left';
    }  
    else if(event.keyCode == 38) {
        direction = 'up';
    }
    else if(event.keyCode == 39) {
        direction = 'right';
    }
    else if(event.keyCode == 40) {
        direction = 'down';
    }

    else if(event.keyCode == 32) {
        gameLoop();
    }


});

function drawGrid() {
    ctx.beginPath();
    ctx.strokeStyle = "grey";
    for(let i = box; i < canvas.height; i = i + box) {
        ctx.moveTo(i, 0);
        ctx.lineTo(i, canvas.height);
    }

    for(let j = box; j < canvas.width; j = j + box) {
        ctx.moveTo(0, j);
        ctx.lineTo(canvas.width, j);
    }
    ctx.stroke();
}

function clear() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    drawGrid();
}

function draw() {
    clear();

    ctx.fillStyle = "green";
    ctx.fillRect(snake[0][0], snake[0][1], box, box);

    ctx.fillStyle = "orange";
    for(let i = 1; i < snake.length; i++) {
        ctx.fillRect(snake[i][0], snake[i][1], box, box);
    }
}

function drawFood(x, y) {
    ctx.fillStyle = "red";
    ctx.fillRect(x, y, box, box);
}

function move() {
    let moved = false;
    let change = [0, 0];

    if(direction == 'left' && snake[0][0] >= 50 && snake[0][0]-box != snake[1][0]) {
        change[0] -= box;
        moved = true;
    } 
    else if(direction == 'up' && snake[0][1] >= 50 && snake[0][1]-box != snake[1][1]) {
        change[1] -= box;
        moved = true;
    }
    else if(direction == 'right' && snake[0][0] <= canvas.width-100 && snake[0][0]+box != snake[1][0]) {
        change[0] += box;
        moved = true;
    }
    else if(direction == 'down' && snake[0][1] <= canvas.height-100 && snake[0][1]+box != snake[1][1]) {
        change[1] += box;
        moved = true;
    }
    
    if(moved) {
        for (var i = snake.length - 1; i >= 1; i--) {
            snake[i] = snake[i-1];     
        }

        let lastX = snake[0][0];
        let lastY = snake[0][1];
        snake[1] = [lastX, lastY];
        
        snake[0][0] += change[0];
        snake[0][1] += change[1];

        for(let i = 1; i < snake.length; i++) {
            if(snake[0][0] == snake[i][0] && snake[0][1] == snake[i][1]) {
                score = i;
                document.getElementById("score").innerHTML = "score: " + score;
                snake.length = i;
                break;
            }
        }
    }
    
    if(snake[0][0] == foodX && snake[0][1] == foodY) {
        score += 1;

        document.getElementById("score").innerHTML = "score: " + score;
        
        snake.push([snake[snake.length-1][0], snake[snake.length-1][1]]);

        while(true) {
            let x = Math.floor(Math.random() * 10) * 50;
            let y = Math.floor(Math.random() * 10) * 50;
            if(foodX != x && foodY != y) {
                foodX = x;
                foodY = y;
                break;
            }
        }
    }
    draw();
    drawFood(foodX, foodY);
}

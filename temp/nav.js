$(".nav-links").css({'width':'0%'});
$(".nav-links").css({'opacity': '1'});
$(".nav-links li a").hide();


$(document).ready(function () {

    // TODO: click for mobile
    // $(".flex-container").mousedown(function(){
    //     $(".nav-links li a").each(function(i){
    //         $(this).delay(100*i).animate({opacity: 'toggle'}, 400);                       
    //     });
    //     $(".nav-links li a").toggle();
    //     $(".menu").animate({opacity: 'toggle'});
    // });
    // $(".flex-container").mousedown(function(){
    //     $(".nav-links").animate({width:'0%'},350);
    //     $(".nav-links li a").animate({opacity: '0'}, 500);
    //     $(".menu").animate({opacity: '1'});
    //     $(".nav-links li a").hide();
    // });

    // TODO: enlarge hovered li
    // FIXME: refactor



    $(".flex-container").hover(function(){
        if($(window).width() > 768) {
            // Fade out 'about;
            $(".menu").css({"padding-left": "0%"})
            $('.menu').css({"visibility": "hidden"});

            // Fade in links
            $(".nav-links li a").show();

            var width = $(window).width();
            if(width > 1024) {
                $(".nav-links").css({'width':'40%'});
            }
            else if(width > 896) {
                $(".nav-links").css({'width':'50%'});
            }
            else{
                $(".nav-links").css({'width':'60%'});
            }
            
            $(".nav-links li a").css({'opacity':'0'});
            // $(".nav-links").animate({widthopacity:'1'},350);
            $(".nav-links li a").each(function(i){
                $(this).delay(200*i).animate({opacity: '1'}, 400);                       
            });
            // $(".nav-links li a").show();
        }
        else {
            // TODO: make covered up shit transparent
            // Fade out 'about;
            $(".menu").css({"padding-left": "0%"})
            $('.menu').css({"visibility": "hidden"});

            // Fade in links
            $('.menu').css({"opacity": "0"});
            $('.nav-links').css({"opacity": "1"});
            $(".nav-links").animate({width:'40%'},350);
            $(".nav-links li a").each(function(i){
                $(this).delay(200*i).animate({opacity: '1'}, 400);                       
            });
            $(".nav-links li a").show();

            // $(".column").animate({opacity: '0'}, 200);
            $('.column').css({"visibility": "hidden"});
        }

        
    },
    function () {
        // FIXME: clear queue of all items, or animations can chain up

        // Desktop
        if($(window).width() > 768) {
            // Fade out links
            $(".nav-links li a").animate({opacity: '0'}, 400);
            $(".nav-links").css({'width':'0%'});

            $(".nav-links li a").hide();
            // $(".menu").css({"padding-left": "70%"})

            // Fade in 'about'
            $(".menu").css({"padding-left": "70%"})
            $('.menu').css({"opacity": "0"});
            $('.menu').css({"visibility": "visible"});
            $(".menu").animate({opacity: '1'}, 800);

            $(".nav-links li a").clearQueue();
            $(".nav-links").clearQueue();
            $(".menu").clearQueue();
        }
        // Mobile
        else {
            // Fade out links
            $(".nav-links li a").animate({opacity: '0'}, 400);
            $(".nav-links").animate({width:'0%'},350);
            $(".nav-links li a").hide();

            // Fade in 'about'
            $(".menu").css({"padding-left": "70%"})
            $('.menu').css({"opacity": "0"});
            $('.menu').css({"visibility": "visible"});
            $(".menu").animate({opacity: '1'}, 800);

            // $(".column").animate({opacity: '1'}, 200);
            $('.column').css({"visibility": "visible"});

            $(".nav-links li a").clearQueue();
            $(".nav-links").clearQueue();
            $(".menu").clearQueue();
        }
    });

});